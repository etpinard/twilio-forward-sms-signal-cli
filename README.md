# twilio-forward-sms-signal-cli

Forward SMS from [Twilio](https://www.twilio.com) using
[signal-cli](https://github.com/AsamK/signal-cli).

For example, register your Twilio phone number for Signal messaging with
signal-cli and use this script on a home server or VPS to forward SMS sent to
your Twilio phone number to the Signal account linked to your phone.

## Installation

```
$ git clone https://gitlab.com/etpinard/twilio-forward-sms-signal-cli.git

# Install Node.js deps
$ node --version
v14.17.6
$ npm i
$ npm i -g twilio-cli@4.1.0

# Install signal-cli and register Twilio phone number
# by following:
# - https://github.com/AsamK/signal-cli#installation
# - https://github.com/AsamK/signal-cli#usage

# Export environment variables
export TWILIO_ACCOUNT_SID=dsafdsfj021i3es
export TWILIO_AUTH_TOKEN=1032wksadsjf0321
export TWILIO_PHONE_NUMBER=+12434213
export PHONE_NUMBER=+11231421
export TWILIO_PROXY_URL=https://your-domain.com/twilio

# Setup reverse-proxy exposed to internet
# e.g. for apache2 (with https support)
$ in /etc/sites-enabled/escale-le-ssl.conf

# ...
ProxyPass /twilo http://127.0.0.1:3000
ProxyPassReverse /twilo http://127.0.0.1:3000
# ...

# ... then restart apache to see effects
$ sudo service apache2 restart
```

## How to run this thing

```
$ npm start
```

### Add crontab

I found that the twilio process would sometimes _hang_ when not receiving any
SMS for multiple hours. So, I recommend restarting the process once every hour
using a crontab to ensure availability throughout the day:

```
$ crontab -e

# then e.g. add this line:
1 * * * *   /path/to/your/copy/of/twilio-forward-sms-signal-cli/crontab.sh

# and also this line for reboots:
@reboot     sleep 5 && /path/to/your/copy/of/twilio-forward-sms-signal-cli/crontab.sh
```

## Additional resources

- https://www.twilio.com/docs/sms/quickstart/node#receive-and-reply-to-inbound-sms-messages-with-express
- https://support.twilio.com/hc/en-us/articles/223134287-Forwarding-SMS-messages-to-another-phone-number

## Code Style

[shellcheck](https://github.com/koalaman/shellcheck)

[![Standard - JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

## License

[MIT](./LICENSE) - etpinard
