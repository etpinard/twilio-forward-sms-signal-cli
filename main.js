const { exec } = require('child_process')

const express = require('express')
const { urlencoded } = require('body-parser')

const TWILIO_PHONE_NUMBER = process.env.TWILIO_PHONE_NUMBER
const PHONE_NUMBER = process.env.PHONE_NUMBER
const TWILIO_PROXY_URL = process.env.TWILIO_PROXY_URL
const PORT = 3000

const ROUTE = 'handle_sms'
const TWILIO_CMD = 'twilio phone-numbers:update'
const TWILIO_OPTS = `--sms-url ${TWILIO_PROXY_URL}/${ROUTE}`

const app = express()
app.use(urlencoded({ extended: false }))

function __exec (cmd, cb) {
  return exec(cmd, (error, stdout, stderr) => {
    if (error) {
      console.error(error)
      return
    }
    cb(stdout, stderr)
  })
}

function _exec (cmd) {
  return __exec(cmd, () => {})
}

function _execAndLog (cmd) {
  return __exec(cmd, (stdout, stderr) => {
    console.log(stdout)
    console.error(stderr)
  })
}

app.post(`/${ROUTE}`, (req, res) => {
  const out = `SMS de ${req.body.From}: ${req.body.Body}`

  console.log(out)
  _exec(`signal-cli -u ${TWILIO_PHONE_NUMBER} receive`)
  _execAndLog(`signal-cli -u ${TWILIO_PHONE_NUMBER} send -m "${out}" ${PHONE_NUMBER}`)

  res.writeHead(200, { 'Content-Type': 'text/xml' })
  res.end()
})

app.listen(PORT, () => {
  console.log(`Express server listening on port ${PORT}`)
  _execAndLog(`${TWILIO_CMD} "${TWILIO_PHONE_NUMBER}" ${TWILIO_OPTS}`)
})
