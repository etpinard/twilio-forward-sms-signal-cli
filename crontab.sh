#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")
HOME="/home/$(whoami)/"
DATE=$(date +"%Y-%m-%d-%H%M%S")
PIDFILE="$HOME/twilio.pid"
LOGFILE="$HOME/twilio.log"

# shellcheck disable=SC1090
source "$HOME/.profile"
export NVM_DIR="$HOME/.nvm"
# shellcheck disable=SC1090
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

_killold () {
  if [ -f "$PIDFILE" ]; then
    echo "killed old process" >> "$LOGFILE"
    pkill -F "$PIDFILE" || true
  fi
}

_main () {
  {
    echo "$DATE"
    command -v node
    command -v twilio
    twilio --version
  } >> "$LOGFILE"
  ( node "$HERE/main.js" 2>&1 & echo $! >&3 ) 3>"$PIDFILE" | tee -a "$LOGFILE"
}

_killold
_main
